# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
  def initialize
    #print gets.chomp how many disk??  << into towers
    @towers = [[3,2,1],[],[]]
  end

  def move(origin_tower, destination_tower)
    disk = towers[origin_tower].pop
    towers[destination_tower].push(disk)
  end

  def valid_move?(origin_tower, destination_tower)
    orig = towers[origin_tower]
    dest = towers[destination_tower]
    return false if orig.empty?
    return false if !dest.empty? && orig.last > dest.last
    true
  end

  def won?
    return true if (towers[2].length == 3 || towers[1].length == 3)
    false
  end

  def play
    until won?
      abstract_move
      puts render
    end
    conclude
  end

  def conclude
    puts "You Won Towers of Hanoi! Congrats!"
    puts "p.s. You're an awesome mom ;)"
  end

  def abstract_move
    puts render
    print "Please select your move (e.g. from to): "
    the_move = gets.chomp!
    origin, destination = the_move.split(' ').map(&:to_i)

    until valid_move?(origin, destination)
      origin, destination = get_valid_move
    end
    move(origin, destination)
  end

  def get_valid_move
    puts render
    print "Invalid Move. Please select your move (e.g. from to): "
    the_move = gets.chomp!
    origin, destination = the_move.split(' ').map(&:to_i)
  end

  def render
    top = towers.map {|tower| tower.length >=3 ? " x " : '   '}
    mid = towers.map {|tower| tower.length >=2 ? " x " : '   '}
    bot = towers.map {|tower| tower.length >=1 ? " x " : '   '}
    "#{top.join(' ')}\n#{mid.join(' ')}\n#{bot.join(' ')}\n___ ___ ___"
  end

  if __FILE__ == $PROGRAM_NAME
    t = TowersOfHanoi.new
    t.play
  end
end
